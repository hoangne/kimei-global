module.exports = {
    trailingComma: "es5",
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: false,
    printWidth: 225,
    bracketSpacing: true,
};
